import React from 'react'
import ReactDOM from 'react-dom'
import { observable } from 'mobx'
import { Provider, observer, inject } from 'mobx-react'

// Basic store for UI state
// The _navIsOpen is just a naming convention to call properties things equivalent to their getter and setter. It could be called _poo for all we care.
class UiStateStore {
  @observable _navIsOpen = false

  // setter to change state
  set navIsOpen (state) {
    this._navIsOpen = state
  }

  // getter to get state
  get navIsOpen () {
    return this._navIsOpen;
  }
}

// New store instance
const UiState = new UiStateStore();

// Inject uiState store to observable class
@inject('uiState') @observer class Navigation extends React.Component {
  constructor (props) {
    super(props)
    this.props = props
  }

  render () {
    return (
      // Classname is based on navIsOpen
      <nav id="nav-example" className={this.props.uiState.navIsOpen ? 'open' : ''}>
        <ul>
          <li><a href="">Test 1</a></li>
          <li><a href="">Test 2</a></li>
          <li><a href="">Test 3</a></li>
        </ul>
      </nav>
    );
  }
}

// Inject uiState store to nav expand, so that it can both set and set
@inject('uiState') @observer class NavigationExpand extends React.Component {
  constructor (props) {
    super(props)
    this.props = props
  }

  // simply switches navIsOpen when clicked
  render () {
    return <a onClick={() => { this.props.uiState.navIsOpen = !this.props.uiState.navIsOpen }}>{this.props.uiState.navIsOpen ? 'Close menu' : 'Open menu'}</a>
  }
}

// Render to DOM, with provider for state
// <Provider /> is a component provided by mobx-react. It is designed specifically to pass data down through components.
// The stores passed can be accessed with the @injector decorator
// This allows us to create essentially stateless components, that have one source of truth
ReactDOM.render(
  <Provider uiState={UiState}>
    <div>
      <NavigationExpand />
      <Navigation />
    </div>
  </Provider>,
  document.getElementById('app-root')
);
