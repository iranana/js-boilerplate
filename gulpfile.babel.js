import gulp from "gulp"
import browserify from "browserify"
import babelify from "babelify"
import uglify from "gulp-uglify"
import source from "vinyl-source-stream"
import buffer from "vinyl-buffer"
import sourcemaps from "gulp-sourcemaps"
import glob from "glob"
import es from "event-stream"
import sass from "gulp-sass"
import webserver from "gulp-webserver"

const vendors = [
  'react',
  'react-dom',
  'prop-types',
  'mobx',
  'mobx-react',
];

//
// Watchers
//
gulp.task("watch:sass", () => {
    gulp.watch("./src/css/**/*.scss", ["build:sass"]);
});
gulp.task("watch:js", () => {
    gulp.watch("./src/js/**/*.js", ["build:js:app"]);
});

//
// Build SASS
//
gulp.task("build:sass", () => {
  return gulp.src("./src/css/**/*.scss")
    .pipe(sass())
    .pipe(gulp.dest("./dist/css/"))
});

//
// Build Vendor.js
//
gulp.task("build:js:vendor", () => {
  const b = browserify({
    debug: true
  });

  vendors.forEach(lib => {
    b.require(lib);
  });

  b.bundle()
  .pipe(source("Vendor.js"))
  .pipe(buffer())
  //.pipe(uglify())
  .pipe(sourcemaps.init({loadMaps: true}))
  .pipe(sourcemaps.write("./maps"))
  .pipe(gulp.dest("./dist/js/"));
});

//
// Build App.js
//
gulp.task("build:js:app", (done) => {
  glob("./src/js/App.js", (err, files) => {
    if(err) {
      done(err);
    }

    var tasks = files.map((entry) => {
      return browserify({
          entries: [entry],
          extensions: [".js", ".jsx"],
          paths: ['./node_modules/', './src/js/', './src/css/'],
          debug: true
        })
        .external(vendors)
        .transform('babelify')
        .transform({global: true}, require("browserify-css"))
        .bundle()
        .pipe(source(entry.split("/").pop()))
        .pipe(buffer())
        //.pipe(uglify())
        .pipe(sourcemaps.init({loadMaps: true}))
        .pipe(sourcemaps.write("./maps"))
        .pipe(gulp.dest("./dist/js/"))
    });

    es.merge(tasks).on("end", done);
  })
});

//
// Webserver
//
gulp.task("webserver", () => {
  gulp.src('./').pipe(webserver({
    livereload: false,
    directoryListing: false,
    open: false,
    port: 9999
  }));
});

gulp.task("build", ["build:sass", "build:js:vendor", "build:js:app"]);
gulp.task("watch", ["watch:js", "watch:sass"]);
gulp.task("default", ["build", "watch", "webserver"]);
